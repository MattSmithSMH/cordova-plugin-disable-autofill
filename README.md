# cordova-plugin-disable-autofill

Cordova plugin for Android to disable autofill on input fields.

## Installation

`cordova plugin add https://bitbucket.org/mattsmithsmh/cordova-plugin-disable-autofill`
