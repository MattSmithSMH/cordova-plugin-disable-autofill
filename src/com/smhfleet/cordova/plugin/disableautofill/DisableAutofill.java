package com.smhfleet.cordova.plugin.disableautofill;

import android.app.Activity;
import android.view.View;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;

public class DisableAutofill extends CordovaPlugin
{
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);

        Activity activity = cordova.getActivity();
        View view = activity.getWindow().getDecorView();

        view.setImportantForAutofill(0x8);
    }
}
